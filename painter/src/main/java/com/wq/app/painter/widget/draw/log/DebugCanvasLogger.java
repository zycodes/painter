package com.wq.app.painter.widget.draw.log;

import android.graphics.Canvas;
import android.graphics.RectF;

import com.wq.app.painter.BuildConfig;


public class DebugCanvasLogger {

    public void log(Canvas canvas, RectF canvasRect, RectF viewRect, float scaleFactor) {
        if (BuildConfig.DEBUG) {
            RectF notScaled = new RectF(canvasRect);
            notScaled.right /= scaleFactor;
            notScaled.bottom /= scaleFactor;
            int lineNumber = 0;

            LogPrinter.log("------------------------------------------------");
            logLine(canvas, "Canvas: " + toShortString(canvasRect), ++lineNumber);
            logLine(canvas, "No scaled canvas: " + toShortString(notScaled), ++lineNumber);
            logLine(canvas, "View: " + toShortString(viewRect), ++lineNumber);
            logLine(canvas, "Scale factor: " + scaleFactor + "x", ++lineNumber);
            LogPrinter.log("------------------------------------------------");
        }
    }

    private static String toShortString(RectF rectf) {
        return "["
                + rectf.left
                + ','
                + rectf.top
                + "]["
                + rectf.right
                + ','
                + rectf.bottom
                + "] B["
                + rectf.width()
                + ","
                + rectf.height()
                + "]";
    }

    private void logLine(Canvas canvas, String text, int lineNumber) {
//    canvas.drawText(text, 5, 30 * lineNumber, paint);
        LogPrinter.log(text);
    }
}
