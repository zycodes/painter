package com.wq.app.painter;


import com.wq.app.painter.data.Stroke;

/**
 * Author: zyc
 * Date  : 9/20/15.
 */
public interface OnStrokeReceiveListener {
    void onStrokeReceived(Stroke stroke);
}
