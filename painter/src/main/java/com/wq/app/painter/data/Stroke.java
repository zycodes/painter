package com.wq.app.painter.data;

import android.graphics.Rect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Author: zyc
 * Date  : 4/25/15.
 */
public class Stroke implements Serializable {
    private static final int STATE_GONE= 0;
    private static final int STATE_VISIBLE = 1;

    /**
     * 笔画的状态
     * 0: STATE_GONE 不显示笔画，即删除这一个笔画
     * 1: STATE_VISIBLE 显示笔画
     */
    private int state;
    /**
     * 此笔画的颜色
     */
    private int color;
    /**
     * 此笔画的粗细
     */
    private float with;
    /**
     * 此笔画中包含的点
     */
    private ArrayList<StrokePoint> points = new ArrayList<>();


    public Stroke(int color, float with) {
        this.color = color;
        this.with = with;
    }

    public ArrayList<StrokePoint> getPoints() {
        return points;
    }

    public void addPoint(StrokePoint point) {
        points.add(point);
    }

    public int getPointCount() {
        return null != points ? points.size() : 0;
    }

    public StrokePoint getLastPoint() {
        return null != points && points.size() > 0 ? points.get(points.size() - 1) : null;
    }

    public float getWith() {
        return with;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void setWith(float with) {
        this.with = with;
    }

    public Rect getArea() {
        Rect rect = new Rect();
        for (StrokePoint point : points) {
            rect.union(point.x, point.y);
        }

        return rect;
    }

    @Override
    public String toString() {
        return "Stroke{" +
                " color: " + Integer.toHexString(color) +
                ", with: " + with +
                ", points: " + Arrays.deepToString(points.toArray()) +
                '}';
    }

    public static class StrokePoint implements Serializable {
        /**
         * 1. 此笔画点相对于前一个点的时间间隔
         * <p/>
         * 2. 当此点为笔画的第一个点时，表示笔画开始的时间
         */
        private long t;
        /**
         * 1. 此笔画点相对于前一个点在X轴上的位置
         * （x>0 在右边，x<0 在左边）
         * <p/>
         * 2. 当此点为笔画的第一个点时，表示笔画开始x坐标
         */
        private int x;
        /**
         * 1. 此笔画点相对于前一个点在Y轴上的位置
         * （y>0 在右边，y<0 在左边）
         * <p/>
         * 2. 当此点为笔画的第一个点时，表示笔画开始的y坐标
         */
        private int y;

        /**
         * constructor method
         *
         * @param t 笔画点相对于前一个点的时间间隔
         * @param x 笔画点相对于前一个点在X轴上的位置
         * @param y 笔画点相对于前一个点在Y轴上的位置
         */
        public StrokePoint(long t, int x, int y) {
            this.t = t;
            this.x = x;
            this.y = y;
        }

        public long getTime() {
            return t;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public void setTime(long t) {
            this.t = t;
        }

        public void setX(int x) {
            this.x = x;
        }

        public void setY(int y) {
            this.y = y;
        }

        @Override
        public String toString() {
            return " (" + x + ", " + y + ") " + t;
        }
    }
}
