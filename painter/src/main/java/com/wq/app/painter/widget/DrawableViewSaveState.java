package com.wq.app.painter.widget;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

import com.wq.app.painter.data.WhiteBoardPage;


public class DrawableViewSaveState extends View.BaseSavedState {

    private WhiteBoardPage boardPage;

    public DrawableViewSaveState(Parcel in) {
        super(in);
        this.boardPage = (WhiteBoardPage) in.readSerializable();
    }

    public DrawableViewSaveState(Parcelable parcelable) {
        super(parcelable);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.boardPage);
    }

    public WhiteBoardPage getBoardPage() {
        return boardPage;
    }

    public void setBoardPage(WhiteBoardPage boardPage) {
        this.boardPage = boardPage;
    }

    public static final Creator<DrawableViewSaveState> CREATOR =
            new Creator<DrawableViewSaveState>() {
                public DrawableViewSaveState createFromParcel(Parcel source) {
                    return new DrawableViewSaveState(source);
                }

                public DrawableViewSaveState[] newArray(int size) {
                    return new DrawableViewSaveState[size];
                }
            };
}
