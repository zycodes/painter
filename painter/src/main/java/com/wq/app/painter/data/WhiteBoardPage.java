package com.wq.app.painter.data;

import java.io.Serializable;
import java.util.Vector;

import com.wq.app.painter.widget.StrokeConfig;

/**
 * Author: zyc
 * Date  : 4/25/15.
 */
public class WhiteBoardPage implements Serializable {
    /**
     * 当前页面所包含的笔画数
     */
    private Vector<Stroke> strokes;
    private Vector<Stroke> localStrokes;
    private Vector<Stroke> remoteStrokes;

    public WhiteBoardPage() {
        this(new Vector<Stroke>());
    }

    public WhiteBoardPage(Vector<Stroke> strokes) {
        this.strokes = strokes;
        localStrokes = new Vector<>();
        remoteStrokes = new Vector<>();
    }

    public void addLocalStroke(Stroke stroke) {
        addStroke(stroke);
        localStrokes.add(stroke);
    }

    public Stroke removeLastLocalStroke() {
        return localStrokes.size() > 0 ? localStrokes.remove(localStrokes.size() - 1) : null;
    }

    public void addRemoteStroke(Stroke stroke) {
        addStroke(stroke);
        remoteStrokes.add(stroke);
    }

    public Stroke removeLastRemoteStroke() {
        return remoteStrokes.size() > 0 ? remoteStrokes.remove(remoteStrokes.size() - 1) : null;
    }

    private void addStroke(Stroke stroke) {
        strokes.add(stroke);
//            LogPrinter.log("add stroke: " + stroke);
    }

    public int getCurrColor() {
        Stroke stroke = getLastStroke();
        return null != stroke ? stroke.getColor() : StrokeConfig.DEFAULT_FONT_COLOR;
    }

    public float getCurrWidth() {
        Stroke stroke = getLastStroke();
        return null != stroke ? stroke.getWith() : StrokeConfig.DEFAULT_FONT_WIDTH;
    }

    public Vector<Stroke> getStrokes() {
        return strokes;
    }

    public void clear() {
        if (null != strokes) {
            strokes.clear();
        }
    }

    public Stroke getLastStroke() {
        return null != strokes && strokes.size() > 0 ? strokes.get(strokes.size() - 1) : null;
    }
}
