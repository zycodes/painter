package com.wq.app.painter.widget.draw;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

import com.wq.app.painter.BuildConfig;
import com.wq.app.painter.widget.StrokeConfig;
import com.wq.app.painter.widget.draw.log.DebugCanvasLogger;

public class CanvasDrawer {
    private boolean showCanvasBounds;
    private Paint paint;
    private float scaleFactor = 1.0f;
    private RectF viewRect = new RectF();
    private RectF canvasRect = new RectF();
    private DebugCanvasLogger canvasLogger;

    public CanvasDrawer() {
        initPaint();
        initLogger();
    }

    public void onDraw(Canvas canvas) {
        if (null != canvasLogger) {
//            canvasLogger.log(canvas, canvasRect, viewRect, scaleFactor);
        }

        if (showCanvasBounds) {
            canvas.drawRect(canvasRect, paint);
        }
//    canvas.scale(scaleFactor, scaleFactor);
    }

    public void onScaleChange(float scaleFactor) {
        this.scaleFactor = scaleFactor;
    }

    public void onViewPortChange(RectF viewRect) {
        this.viewRect = viewRect;
    }

    public void onCanvasChanged(RectF canvasRect) {
        this.canvasRect = canvasRect;
    }

    private void initPaint() {
        paint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG | Paint.FILTER_BITMAP_FLAG);
        paint.setColor(Color.RED);
        paint.setStrokeWidth(6.0f);
        paint.setStyle(Paint.Style.STROKE);
    }

    private void initLogger() {
        if (BuildConfig.DEBUG) {
            canvasLogger = new DebugCanvasLogger();
        }
    }

    public void setConfig(StrokeConfig config) {
        this.showCanvasBounds = config.isShowCanvasBounds();
    }
}
