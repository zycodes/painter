package com.wq.app.painter.widget.gestures.creator;


import com.wq.app.painter.data.Stroke;

public interface GestureCreatorListener {
    void onGestureCreated(Stroke stroke);

    void onCurrentGestureChanged(Stroke currStroke);
}
