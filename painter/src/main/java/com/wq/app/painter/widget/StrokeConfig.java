package com.wq.app.painter.widget;

import java.io.Serializable;

public class StrokeConfig implements Serializable {
    public static final float TOUCH_TOLERANCE = 10;
    public static final float DEFAULT_FONT_WIDTH = 16f;
    public static final int DEFAULT_FONT_COLOR = 0xff009688;
    public static final int DEFAULT_BACKGROUND_COLOR = 0xFFF5F5F5;
    private float strokeWidth;
    private int strokeColor = 0xff009688;
    private int canvasWidth;
    private int canvasHeight;
    private float minZoom;
    private float maxZoom;
    private boolean showCanvasBounds;

    public float getMaxZoom() {
        return maxZoom;
    }

    public void setMaxZoom(float maxZoom) {
        this.maxZoom = maxZoom;
    }

    public float getMinZoom() {
        return minZoom;
    }

    public void setMinZoom(float minZoom) {
        this.minZoom = minZoom;
    }

    public int getCanvasHeight() {
        return canvasHeight;
    }

    public void setCanvasHeight(int canvasHeight) {
        this.canvasHeight = canvasHeight;
    }

    public int getCanvasWidth() {
        return canvasWidth;
    }

    public void setCanvasWidth(int canvasWidth) {
        this.canvasWidth = canvasWidth;
    }

    public float getStrokeWidth() {
        return strokeWidth;
    }

    public void setStrokeWidth(float strokeWidth) {
        this.strokeWidth = strokeWidth;
    }

    public int getStrokeColor() {
        return strokeColor;
    }

    public void setStrokeColor(int strokeColor) {
        this.strokeColor = strokeColor;
    }

    public boolean isShowCanvasBounds() {
        return showCanvasBounds;
    }

    public void setShowCanvasBounds(boolean showCanvasBounds) {
        this.showCanvasBounds = showCanvasBounds;
    }
}
