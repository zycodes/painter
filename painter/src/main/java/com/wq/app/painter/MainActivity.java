package com.wq.app.painter;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;

import com.wq.app.painter.widget.DrawableView;
import com.wq.app.painter.widget.StrokeConfig;


public class MainActivity extends Activity {
    private StrokeConfig config = new StrokeConfig();
    private DrawableView drawableView;
    private DisplayMetrics displayMetrics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        displayMetrics = getResources().getDisplayMetrics();

        drawableView = (DrawableView) findViewById(R.id.canvasView);
        config.setStrokeColor(StrokeConfig.DEFAULT_FONT_COLOR);
        config.setShowCanvasBounds(true);
        config.setStrokeWidth(14.0f);
        config.setMinZoom(1.0f);
        config.setMaxZoom(3.0f);
        config.setCanvasHeight(getScreenHeight());
        config.setCanvasWidth(getScreenWidth());
        drawableView.setConfig(config);

        drawableView.requestFocus();
    }

    private int getScreenWidth() {
        return displayMetrics.widthPixels;
    }

    private int getScreenHeight() {
        return displayMetrics.heightPixels;
    }
}
