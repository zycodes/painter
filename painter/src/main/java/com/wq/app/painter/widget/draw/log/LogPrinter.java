package com.wq.app.painter.widget.draw.log;

import android.util.Log;

/**
 * Author: zyc
 * Date  : 8/31/15.
 */
public class LogPrinter {
    public static void log(String msg) {
        Log.d("painter", msg);
    }
}
