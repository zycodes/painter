package com.wq.app.painter;

import android.graphics.Canvas;
import android.os.HandlerThread;
import android.view.SurfaceHolder;

import com.wq.app.painter.widget.DrawableView;

/**
 * Author: zyc
 * Date  : 8/31/15.
 */
public class StrokeDrawerThread extends HandlerThread {
    private static boolean sDrawing = false;
    private DrawableView canvasView;
    private SurfaceHolder surfaceHolder;

    public StrokeDrawerThread(DrawableView drawableView, SurfaceHolder holder) {
        super("StrokeDrawerThread");
        this.surfaceHolder = holder;
        this.canvasView = drawableView;
    }

    public static void setDrawing(boolean drawing) {
        sDrawing = drawing;
    }

    @Override
    public void run() {
        Canvas canvas = null;
        while (sDrawing) {
            if (null != surfaceHolder) {
                try {
                    canvas = surfaceHolder.lockCanvas();
                    synchronized (surfaceHolder) {
                        if (null != canvasView && null != canvas) {
                            canvasView.draw(canvas);
                        }
                    }
                } finally {
                    if (null != canvas) {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        }
    }
}
