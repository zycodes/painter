package com.wq.app.painter.widget.gestures.scale;

public interface ScalerListener {
  void onScaleChange(float scaleFactor);
}
