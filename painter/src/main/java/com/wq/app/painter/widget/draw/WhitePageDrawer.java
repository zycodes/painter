package com.wq.app.painter.widget.draw;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

import java.util.Vector;

import com.wq.app.painter.data.Stroke;
import com.wq.app.painter.data.WhiteBoardPage;
import com.wq.app.painter.widget.StrokeConfig;
import com.wq.app.painter.widget.draw.log.LogPrinter;
import com.wq.app.painter.widget.gestures.scale.ScalerListener;
import com.wq.app.painter.widget.gestures.scroller.ScrollerListener;

/**
 * Author: zyc
 * Date  : 9/14/15.
 */
public class WhitePageDrawer implements ScalerListener, ScrollerListener {
    /**
     * draw the white board background
     */
    private CanvasDrawer canvasDrawer;
    /**
     * draw the stroke (foreground drawer)
     */
    private Vector<StrokeDrawer> cachedStrokeDrawers;

    private Paint pagePaint;
    private RectF viewRect;
    private int strokeStartIndex;


    public WhitePageDrawer() {
        canvasDrawer = new CanvasDrawer();
        cachedStrokeDrawers = new Vector<>();
    }

    public void setConfig(StrokeConfig config) {
        canvasDrawer.setConfig(config);
    }

    public void onDraw(Canvas canvas, WhiteBoardPage page, Bitmap prevBitmap) {
        //draw canvas
        canvasDrawer.onDraw(canvas);

        //draw strokes
        canvas.translate(-viewRect.left, -viewRect.top);
        drawPage(canvas, page, true);
    }

    public Bitmap obtainBitmap(Bitmap createdBitmap, WhiteBoardPage page) {
        Canvas composeCanvas = new Canvas(createdBitmap);
        drawPage(composeCanvas, page, true);
        return createdBitmap;
    }

    @Override
    public void onScaleChange(float scaleFactor) {
        canvasDrawer.onScaleChange(scaleFactor);
    }

    @Override
    public void onViewPortChange(RectF currentViewport) {
        viewRect = currentViewport;
        canvasDrawer.onViewPortChange(currentViewport);
    }

    @Override
    public void onCanvasChanged(RectF canvasRect) {
        initCanvasBitmap((int) canvasRect.width(), (int) canvasRect.height());
        canvasDrawer.onCanvasChanged(canvasRect);
    }

    private void drawPage(Canvas canvas, WhiteBoardPage page, boolean drawDirectly) {
        Vector<Stroke> strokes = page.getStrokes();
        int size = strokes.size();
        if (size > 0) {
            LogPrinter.log(" drawPage stroke size:" + size);

            StrokeDrawer strokeDrawer;
            for (int index = 0; index < size; index++) {
                strokeDrawer = getStrokeDrawer(index, strokes);

                //提升绘制效率，屏幕之外的一律不绘制
                if (isStrokeInViewport(strokeDrawer.getStrokeArea())) {
                    strokeDrawer.onDraw(canvas);
                }
            }
        }
    }

    private boolean isStrokeInViewport(Rect area) {
        return viewRect.intersects(area.left, area.top, area.right, area.bottom);
    }

    private void initCanvasBitmap(int width, int height) {
        pagePaint = new Paint();
        pagePaint.setAntiAlias(true);
        pagePaint.setFilterBitmap(true);
//        pageBitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888);
    }

    private StrokeDrawer getStrokeDrawer(int index, Vector<Stroke> strokes) {
        StrokeDrawer strokeDrawer;
        if (index < cachedStrokeDrawers.size() && null != cachedStrokeDrawers.get(index)) {
            strokeDrawer = cachedStrokeDrawers.get(index);
        } else {
            strokeDrawer = new StrokeDrawer(strokes.get(index));
            cachedStrokeDrawers.add(index, strokeDrawer);
        }

        return strokeDrawer;
    }


}
